package org.emidssoft.services;

import java.util.Scanner;

public class InsurancePremiumCalculator {

	public static void main(String[] args) {
		String insuredName = null;
		String gender = null;
		int insureAge = 0;
		Scanner sc = new Scanner(System.in);
		// input the for the personal details.
		System.out.print("Name :");
		insuredName = sc.nextLine();
		System.out.print("Gendar :");
		gender = sc.nextLine();
		System.out.print("Age :");
		insureAge = Integer.parseInt(sc.nextLine());
		// calculating the Current Health Details		
		String hypertension = null;
		String bloodPresure = null;
		String bloodSugar = null;
		String overWeight = null;
		System.out.println("Current health: ");
		System.out.print("Hypertension: ");
		hypertension = sc.nextLine();
		System.out.print("Blood pressure: ");
		bloodPresure = sc.nextLine();
		System.out.print("Blood sugar: ");
		bloodSugar = sc.nextLine();
		System.out.print("Overweight: ");
		overWeight = sc.nextLine();
		// Calculate the habits of the person
		String smoking = null;
		String alcohol = null;
		String dailyExercise = null;
		String drugs = null;
		System.out.println("Habits: ");
		System.out.print("Smoking: ");
		smoking = sc.nextLine();
		System.out.print("Alcohol: ");
		alcohol = sc.nextLine();
		System.out.print("Daily exercise: ");
		dailyExercise = sc.nextLine();
		System.out.print("Drugs: ");
		drugs = sc.nextLine();
		double basePrimiumPrice = 5000;
		int premiumPeriod = 0;
		System.out.print("Policy Period ");
		premiumPeriod = sc.nextInt();
InsurancePremiumCalculator in=new InsurancePremiumCalculator();
		double totalPremiumCalculate = in.primiumCalculateOnAge(insureAge, basePrimiumPrice, premiumPeriod);

		// calculate premium on gender
		totalPremiumCalculate = in.premiumCalculateOnGender(totalPremiumCalculate, basePrimiumPrice, gender);

		// calculate premium on the pre condition based.

		totalPremiumCalculate = in.premiumCalculateOnHBPOSO(totalPremiumCalculate, basePrimiumPrice, hypertension,
				bloodPresure, bloodSugar, overWeight);

		// calculate premium on the Habits
		totalPremiumCalculate = in.premiumCalculateOnBadHabits(totalPremiumCalculate, basePrimiumPrice, smoking, alcohol,
				drugs);
		totalPremiumCalculate = in.premiumCalculateOnGoodHabits(totalPremiumCalculate, basePrimiumPrice, dailyExercise,
				smoking, alcohol, drugs);
		
		System.out.println("Health Insurance Premium for Mr."+insuredName+":"+totalPremiumCalculate);

	}

	public double premiumCalculateOnGoodHabits(double totalPremiumCalculate, double basePrimiumPrice,
			String dailyExercise, String smoking, String alcohol, String drugs) {
		double calculateTotalOnGoodH = totalPremiumCalculate;
		if (dailyExercise.equals("Yes")) {
			calculateTotalOnGoodH = calculateTotalOnGoodH - (basePrimiumPrice * 0.03);
			if (smoking.equals("No")) {
				calculateTotalOnGoodH = calculateTotalOnGoodH - (basePrimiumPrice * 0.03);
			}
			if (alcohol.equals("No")) {
				calculateTotalOnGoodH = calculateTotalOnGoodH - (basePrimiumPrice * 0.03);
			}
			if (drugs.equals("No")) {
				calculateTotalOnGoodH = calculateTotalOnGoodH - (basePrimiumPrice * 0.03);
			}
			return calculateTotalOnGoodH;
		} else {
			return calculateTotalOnGoodH;
		}
	}

	public  double premiumCalculateOnBadHabits(double totalPremiumCalculate, double basePrimiumPrice,
			String smoking, String alcohol, String drugs) {
		double calculatePremiumOnHabit = totalPremiumCalculate;
		if (smoking.equals("Yes")) {
			calculatePremiumOnHabit = calculatePremiumOnHabit + (basePrimiumPrice * 0.03);
		}
		if (alcohol.equals("Yes")) {
			calculatePremiumOnHabit = calculatePremiumOnHabit + (basePrimiumPrice * 0.03);
		}
		if (drugs.equals("Yes")) {
			calculatePremiumOnHabit = calculatePremiumOnHabit + (basePrimiumPrice * 0.03);
		}
		return calculatePremiumOnHabit;
	}

	public  double premiumCalculateOnHBPOSO(double totalPremiumCalculate, double basePrimiumPrice,
			String hypertension, String bloodPresure, String bloodSugar, String overWeight) {
		double calculatePremiumOnCodition = totalPremiumCalculate;
		if (hypertension.equals("Yes")) {
			calculatePremiumOnCodition = calculatePremiumOnCodition + (basePrimiumPrice * 0.01);
		}
		if (bloodPresure.equals("Yes")) {
			calculatePremiumOnCodition = calculatePremiumOnCodition + (basePrimiumPrice * 0.01);
		}
		if (bloodPresure.equals("Yes")) {
			calculatePremiumOnCodition = calculatePremiumOnCodition + (basePrimiumPrice * 0.01);
		}
		if (overWeight.equals("Yes")) {
			calculatePremiumOnCodition = calculatePremiumOnCodition + (basePrimiumPrice * 0.01);
		}
		return calculatePremiumOnCodition;
	}

	  double premiumCalculateOnGender(double totalPremiumCalculate, double basePrimiumPrice,
			String gender) {
		if (gender.equals("Males")) {
			return totalPremiumCalculate + (basePrimiumPrice * 0.02);
		} else {
			return totalPremiumCalculate;
		}
	}

	  double primiumCalculateOnAge(int insureAge, double basePrimiumPrice, int premiumPeriod) {
		double premiumCalByAge = 0;
		if (insureAge >= 40) {
			if (premiumPeriod > 5) {
				premiumCalByAge = basePrimiumPrice + (basePrimiumPrice * 0.20);
				for (int index = 5; index < basePrimiumPrice + 1; index = index + 5)
					premiumCalByAge = premiumCalByAge + (premiumCalByAge * 0.20);
			} else {
				premiumCalByAge = basePrimiumPrice + (basePrimiumPrice * 0.20);
			}
			return premiumCalByAge;
		}
		if (insureAge > 18 & insureAge < 40) {
			if (premiumPeriod > 5) {
				premiumCalByAge = basePrimiumPrice + (basePrimiumPrice * 0.10);
				for (int index = 5; index < basePrimiumPrice + 1; index = index + 5)
					premiumCalByAge = premiumCalByAge + (premiumCalByAge * 0.10);
			} else {
				premiumCalByAge = basePrimiumPrice + (basePrimiumPrice * 0.10);
			}
			return premiumCalByAge;
		} else {
			return basePrimiumPrice;
		}
	}

}
